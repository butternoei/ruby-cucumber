module GradeStepHelper
   def isPass(score)
      if score.to_i >= 50
         'Pass'
      else
         'Fail'
      end
   end
      
   def grade(score)
      if score >= 80
         'A'
      elsif score >= 75
         'B+'
      elsif score >= 70
         'B'
      elsif score >= 65
         'C+'
      elsif score >= 60
         'C'
      elsif score >= 55
         'D+'
      elsif score >= 50
         'D'
      else
         'F'
      end
   end
end
Feature: Handle Grade?
   Everybody wants to know grade

   Scenario: Score over 49 is pass
      Given Score is 50
      When I ask whether score 50 is pass yet
      Then I should be told "Pass"

   Scenario: Score less than 50 fails
      Given Score is 49
      When I ask whether score 49 is pass yet
      Then I should be told "Fail"

   Scenario: Score over 79 is A grade
      Given Score is 80
      When I ask whether, I get "A" grade?
      Then I should get "A" grade
      
   Scenario: Score over 74 is B+ grade
      Given Score is 75
      When I ask whether, I get "B+" grade?
      Then I should get "B+" grade
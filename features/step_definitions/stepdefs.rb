require_relative '../../modules/grade'

World GradeStepHelper

Given("Score is {int}") do |given_score|
  @score = given_score
end

# Score is pass

When("I ask whether score {int} is pass yet") do |score_is_pass|
  @actual_answer = isPass(@score)
end

Then("I should be told {string}") do |expected_answer|
  expect(@actual_answer).to eq(expected_answer)
end

# Grade calculate

When("I ask whether, I get {string} grade?") do |get_grade|
  @actual_answer = grade(@score)
end

Then("I should get {string} grade") do |expected_answer|
  expect(@actual_answer).to eq(expected_answer)
end